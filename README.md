## Introduction

This is a example from Zend Framework website, but build around Doctrine 2.
The example is about Create Read Update and Delete Albums.
This should give you head start into making web applications using Zend Framework with Doctrine 2.


## Installation using Composer

Basically after you cloned this repo, you just type:

```bash
$ composer update
```

It'll download and or update all the packages for you.


## Web server setup

### Apache setup

To setup apache, setup a virtual host to point to the public/ directory of the
project and you should be ready to go! It should look something like below:

```apache
<VirtualHost *:80>
    ServerName zfapp.localhost
    DocumentRoot /path/to/zfapp/public
    <Directory /path/to/zfapp/public>
        DirectoryIndex index.php
        AllowOverride All
        Order allow,deny
        Allow from all
        <IfModule mod_authz_core.c>
        Require all granted
        </IfModule>
    </Directory>
</VirtualHost>
```

You also need to go to your host file and add this:

```
127.0.0.1 zfapp.localhost localhost
```

*Settings and setup process may vary per operating system!*

## Using docker-compose

This skeleton provides a `docker-compose.yml` for use with
[docker-compose](https://docs.docker.com/compose/); it
uses the `Dockerfile` provided as its base. Build and start the image using:

```bash
$ docker-compose up -d --build
```

At this point, you can visit http://localhost:8080 to see the site running.

You can also run composer from the image. The container environment is named
"zf", so you will pass that value to `docker-compose run`:

```bash
$ docker-compose run zf composer install
```
