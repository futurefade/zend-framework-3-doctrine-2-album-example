<?php
/**
 * Created by PhpStorm.
 * User: work
 * Date: 05/01/2019
 * Time: 11:52
 */

namespace Application\Controller;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class AbstractControllerFactory implements FactoryInterface
{
    private $DOCTRINE_DEFAULT = 'doctrine.entitymanager.orm_default';

    /**
     * Get the Doctrine Entitymanager and initialized controllers with it.
     * @param ContainerInterface $container
     * @param $requestedName
     * @param array|null $options
     *
     * @return mixed
     */
    public function __invoke(ContainerInterface $container, $requestedName, Array $options = null) {
        $entityManager = $container->get($this->DOCTRINE_DEFAULT);
        return new $requestedName($entityManager);
    }

}