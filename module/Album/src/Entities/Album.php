<?php
/**
 * Created by PhpStorm.
 * User: work
 * Date: 04/01/2019
 * Time: 16:04
 */

namespace Album\Entities;

use Application\Entities\AbstractEntity;


use Doctrine\ORM\Mapping as ORM;


/**
 * Album Entity
 *
 * @ORM\Table(name="album")
 * @ORM\Entity(repositoryClass="Doctrine\ORM\EntityRepository")
 */
class Album extends AbstractEntity
{
    function __construct(Array $newAlbum = null)
    {
        $this->setTitle($newAlbum["title"]);
        $this->setArtist($newAlbum["artist"]);
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string", length=100, nullable=false)
     */
    private $title;

    /**
     * @ORM\Column(name="artist", type="string", length=100, nullable=false)
     */
    private $artist;

    /**
     * Returns ID of this album.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns title.
     *
     * @return String
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets title.
     *
     * @param $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns artist.
     *
     * @return String
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * Sets Artist.
     *
     * @param $artist
     */
    public function setArtist($artist)
    {
        $this->artist = $artist;
    }


}