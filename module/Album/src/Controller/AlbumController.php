<?php
/**
 * Created by PhpStorm.
 * User: work
 * Date: 04/01/2019
 * Time: 14:33
 */

namespace Album\Controller;

use Album\Entities\Album;
use Album\Form\AlbumAddEditForm;
use Album\Form\AlbumDeleteForm;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;

/**
 * Controller for handling actions from the view folder named: Album.
 * This controller has CRUD for Album Entity using Doctrine Entity Manager.
 * This Entity Manager is initialized using the AbstractControllerFactory.
 * You can find controller factories in module.config.php.
 *
 * Class AlbumController
 * @package Album\Controller
 */
class AlbumController extends AbstractRestfulController
{
    /**
     * Doctrine Entity Manager.
     *
     * @var EntityManager $entityManager
     */
    private $entityManager;

    /**
     * String that contains the Doctrine Entity named: Album.
     * This is here for easier name change without going through whole document.
     *
     * @var String $albumEntityString
     */
    private $albumEntityString = "Album\Entities\Album";

    /**
     * AlbumController constructor.
     * Initializing $entityManager for local use.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Returns all Albums from the database as albums.
     *
     * @return ViewModel
     */
    public function indexAction()
    {
        return new ViewModel(["albums"=>$this->entityManager->getRepository($this->albumEntityString)->findAll()]);
    }

    /**
     * Add new Album to the database!
     * If there is no post or the form is not valid it'll return to the form.
     *
     * @return array|\Zend\Http\Response
     */
    public function addAction()
    {
        /**
         * @var \Zend\Http\Request $request
         */

        $request = $this->getRequest();

        $form = new AlbumAddEditForm($this->entityManager);
        $form->get('submit')->setValue('Add');

        if (!$request->isPost()) {
            return ['form' => $form];
        }

        $form->setData($request->getPost());

        if (! $form->isValid()) {
            print_r($form->getMessages()); //error messages
            return ['form' => $form];
        }

        try {
            $this->entityManager->persist($form->getObject());
            $this->entityManager->flush();
        } catch (ORMException $e) {
            print_r($e);
        }

        return $this->redirect()->toRoute('album');
    }

    /**
     * Edit existing Album and send it to the database!
     * If there is no post or the form is not valid it'll return to the form.
     * Also if the retrieved album is not an instance of Album it'll throw an Exception.
     *
     * @return array|\Zend\Http\Response
     * @throws \Exception
     */
    public function editAction()
    {
        /**
         * @var \Zend\Http\Request $request
         */

        $id = (int) $this->params()->fromRoute('id', 0);


        if (0 === $id) {
            return $this->redirect()->toRoute('album', ['action' => 'add']);
        }


        $album = $this->entityManager->getRepository($this->albumEntityString)->find($id);

        if (!$album instanceof Album) {
            throw new \Exception('Something went wrong retrieving this album, please try again or contact us.');
        }

        $form = new AlbumAddEditForm($this->entityManager);
        $form->bind($album);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        $viewData = ['id' => $id, 'form' => $form];

        if (! $request->isPost()) {
            return $viewData;
        }

        $form->setData($request->getPost());

        if (! $form->isValid()) {
            print_r($form->getMessages()); //error messages
            return ['form' => $form];
        }

        // Redirect to album list
        return $this->redirect()->toRoute('album', ['action' => 'index']);
    }

    /**
     * Delete existing album from the database!
     * If form is invalid, valid, but user clicked on no, valid, but user clicked on yes, it'll return to index.
     * Also if the retrieved album is not an instance of Album it'll throw an Exception.
     * By default it'll return the existing album and an new instance of the delete form.
     *
     * @return array|\Zend\Http\Response
     * @throws \Exception
     */
    public function deleteAction()
    {
        /**
         * @var \Zend\Http\Request $request
         */

        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('album');
        }

        $album = $this->entityManager->getRepository($this->albumEntityString)->find($id);

        if (!$album instanceof Album) {
            throw new \Exception('Something went wrong retrieving this album, please try again or contact us.');
        }

        $form = new AlbumDeleteForm();

        $request = $this->getRequest();

        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                if (array_key_exists("yes", $request->getPost('delete'))) {
                    try {
                        $this->entityManager->remove($album);
                        $this->entityManager->flush();
                    } catch (ORMException $e) {
                        print_r($e);
                    }
                }
            }

            // Redirect to list of albums
            return $this->redirect()->toRoute('album');
        }

        return array(
            'album' => $album,
            "form"  => $form
        );
    }
}