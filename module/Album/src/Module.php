<?php
/**
 * Created by PhpStorm.
 * User: work
 * Date: 04/01/2019
 * Time: 14:19
 */

namespace Album;

use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
}