<?php
/**
 * Created by PhpStorm.
 * User: work
 * Date: 06/01/2019
 * Time: 13:32
 */

namespace Album\Form;


use Album\Entities\Album;
use Application\Form\AbstractForm;
use Doctrine\ORM\EntityManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\Filter\ToInt;
use Zend\InputFilter\InputFilter;
use Zend\Validator\StringLength;

/**
 * Form that primarely Create new Albums or Edit existing Albums.
 * This form uses Doctrine Hydrator and set a new Doctrine Entity Album as object.
 * This form also set its own Inputfilter, instead of a separate class.
 *
 * @package Album\Form
 */
class AlbumAddEditForm extends AbstractForm
{
    /**
     * AlbumAddEditForm constructor.
     * Initializing elements, input filter, hydrator and a Doctrine Entity Album as this form Object.
     *
     * @param EntityManager $entityManager
     * @param string $name
     * @param array $options
     */
    public function __construct(EntityManager $entityManager, $name = "Album-Add-Edit-Form", $options = [])
    {
        // We will ignore the name provided to the constructor
        parent::__construct($name, $options);


        // Set POST method for this form
        $this->setAttribute('method', 'post');

        $this->addElements();
        $this->addInputFilter();

        //Set Doctrine Object as Hydrator
        $this->setHydrator(new DoctrineObject($entityManager));
        //Set Doctrine Entity
        $this->setObject(new Album());
    }

    /**
     * Initialize Form Elements.
     * This is later called from phtml views.
     */
    private function addElements()
    {
        $this->add([
            'name' => 'id',
            'type' => 'hidden',
        ]);
        $this->add([
            'name' => 'title',
            'type' => 'text',
            'options' => [
                'label' => 'Title',
            ],
        ]);
        $this->add([
            'name' => 'artist',
            'type' => 'text',
            'options' => [
                'label' => 'Artist',
            ],
        ]);
        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Go',
                'id'    => 'submitbutton',
            ],
        ]);
    }

    /**
     * Initialize Form Input filter
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name' => 'id',
            'required' => true,
            'filters' => [
                ['name' => ToInt::class],
            ],
        ]);

        $inputFilter->add([
            'name' => 'artist',
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 100,
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'title',
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 100,
                    ],
                ],
            ],
        ]);
    }
}