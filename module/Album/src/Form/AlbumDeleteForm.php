<?php
/**
 * Created by PhpStorm.
 * User: work
 * Date: 14/01/2019
 * Time: 09:58
 */

namespace Album\Form;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;

class AlbumDeleteForm extends AbstractForm
{
    /**
     * AlbumDeleteForm constructor.
     * Initializing elements and input filter.
     *
     * @param string $name
     * @param array $options
     */
    public function __construct($name = "Album-Delete-Form", $options = [])
    {
        parent::__construct($name, $options);

        $this->addElements();
        $this->setInputFilter(new InputFilter());
    }

    /**
     * Initialize Form Elements.
     * This is later called from phtml views.
     */
    public function addElements()
    {
        $this->add([
            'name' => 'id',
            'type' => 'hidden',
        ]);
        $this->add([
            'name'       => 'delete[yes]',
            'type'       => 'submit',
            'attributes' => [
                'name' => 'delete[yes]',
                'value'       => 'Yes',
                'data-action' => 'delete',
            ],
        ]);
        $this->add([
            'name'       => 'delete[no]',
            'type'       => 'submit',
            'attributes' => [
                'name' => 'delete[no]',
                'value'       => 'No',
                'data-action' => 'cancel',
            ],
        ]);
    }
}