<?php
/**
 * Created by PhpStorm.
 * User: work
 * Date: 05/01/2019
 * Time: 11:37
 */

use Doctrine\DBAL\Driver\PDOMySql\Driver as PDOMySqlDriver;

return [
    'application' => [
        'name' => 'Zend Framework Tutorial',
    ],
    'host' => 'zend',
    'doctrine' => [
        'configuration' => [
            'orm_default' => [
                'generate_proxies' => false,
            ],
        ],
        'connection' => [
            'orm_default' => [
                'driverClass' => PDOMySqlDriver::class,
                'params' => [
                    'dbname'   => 'album',
                    'user'     => 'user',
                    'password' => 'userpermissions',
                    'host'     => 'localhost',
                    'port'     => '3306',
                    'charset'  => 'utf8mb4',
                    'driverOptions' => [
                        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4'
                    ]
                ],
            ],
        ],
    ],
];